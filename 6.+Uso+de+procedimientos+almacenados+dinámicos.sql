use bd_pedido
go

/*
	6. Uso de procedimientos almacenados dinámicos
*/

-- Listar Clientes enviando dinámicamente sus ID


Alter Proc Usp_cliente_parametro
@cId varchar(100)
as
begin
	declare @sql nvarchar(4000) -- Varchar (8000) VS Nvarchar (4000) 

	set @sql = 'select * from tb_cliente Where IdCliente in (' +  @cId + ')'

	exec (@sql)
end
go

exec Usp_cliente_parametro '1, 20 ,23'


--generar un Backup de forma dinamica enviando como parametro cualquier tabla 

alter Proc Usp_Backup_Table_Dinamic
@cTB_Original varchar(100),
@cTB_Salida varchar(100)
as

   declare @sql nvarchar(4000) -- Varchar (8000) VS Nvarchar (4000) 

	set @sql = 'select * 
				Into [' + @cTB_Salida  + '] 
				from ' + @cTB_Original 

	exec (@sql)
go


exec Usp_Backup_Table_Dinamic 'tb_empleado', 'tb_empleado_bk_2026-03-26'


--- Contar Registros de una tabla 
Alter Proc Usp_Cantidad_Reg_Table_Dinamic
@cNom_Tabla varchar(100)
as

   declare @sql nvarchar(4000) -- Varchar (8000) VS Nvarchar (4000) 

	set @sql = 'select Count(1) as Resultado
				from ' + @cNom_Tabla 

	exec (@sql)
go

exec Usp_Cantidad_Reg_Table_Dinamic 'tb_pedido'



----------------------------------------------------------------------------
alter Proc Usp_Cantidad_Reg_Table_Dinamic_Output
@cNom_Tabla  varchar(100),
@cCantidad int OutPUT
as
 DECLARE @cVarTable Table ( Cantidad int  )
   declare @sql nvarchar(4000) -- Varchar (8000) VS Nvarchar (4000) 

	set @sql = 'select Count(1) as Resultado
				from ' + @cNom_Tabla 

	insert into @cVarTable
	exec (@sql)

 set @cCantidad = (select Cantidad  from @cVarTable )
 --select @cCantidad
go

declare @cCantidad int
exec Usp_Cantidad_Reg_Table_Dinamic_Output 'tb_cliente', @cCantidad out

-----------------------------------------------------------------------------
drop table if exists #Temporal
Select ROW_NUMBER() over(order by table_name asc) as Correlativo, table_name
into #Temporal
from INFORMATION_SCHEMA.TABLES 
where Not table_name in ('sysdiagrams','tb_cliente_bk_2022-03-26','tb_clientes_bk_2022-03-26','tb_empleado_bk_2026-03-26')

alter table #Temporal 
add CantidadReg Int

select * from #Temporal
 
-----------------------------------------------------------------
Declare @cinicio int
Declare @cFin int 
set @cinicio = 1
set @cFin = (Select MAX(Correlativo) from #Temporal )

While (@cinicio <= @cFin)
Begin
	
	declare @ctable_name varchar(50)
	set @ctable_name = (Select table_name from #Temporal where Correlativo = @cinicio)
		 
	declare @cCantidadReg int 
	exec Usp_Cantidad_Reg_Table_Dinamic_Output @ctable_name, @cCantidadReg out

	update #Temporal 
	set CantidadReg = @cCantidadReg
	where Correlativo = @cinicio

	set @cinicio = @cinicio + 1
End
go

