/*
	7. Uso de vistas:
	Las vistas en SQL son una especie de tablas virtuales. 
	Una vista tambi�n tiene filas y columnas, ya que est�n en una tabla real en la base de datos.

*/

-- 1. Crear una vista que liste los productos con su nombre de categoria.

Create View Vw_Producto_Categoria
As
select a.nombreProducto, b.nombrecategoria
from tb_producto a inner join tb_categoria b 
on a.idCategoria = b.idcategoria 
go

select * from Vw_Producto_Categoria

-- 2. Crear una vista que muestre la cantidad de pedidos de los clientes.

Create View Vw_Cliente_Pedido
As
select b.Nombre_empresa, b.CargoContacto ,COUNT(1) As Cantidad
from tb_pedido a inner join tb_cliente b 
on a.IdCliente = b.IdCliente 
Group by b.Nombre_empresa, b.CargoContacto

select * from Vw_Cliente_Pedido
order by 1 asc

-- 3. Crear una vista que muestre solo los campos "nombre_empresa, nombrecontacto, cargocontacto, telefono" de los proveedores. 

Create View Vw_Proveedor
as
select nombre_empresa, nombrecontacto, cargocontacto, telefono
from tb_proveedor 

select * from Vw_Proveedor

-- 4. Elabore la vista que contenga el n�mero de ventas producidas por un vendedor (empleado) clasificadas por mes y 
--seleccionadas de un a�o determinado (2019)

Create View Vw_Pedido_cliente_MES
as
select b.Nombre, b.Apellidos,  MONTH(a.FechaPedido) as Mes, COUNT(1) As Cantidad
from tb_pedido a inner join tb_empleado b 
on a.IdEmpleado = b.IdEmpleado 
where YEAR(a.FechaPedido) = 2019
Group by  b.Nombre, b.Apellidos,  MONTH(a.FechaPedido)
 
 select * from Vw_Pedido_cliente_MES

-- 5. Determine cual es el producto m�s vendido, del a�o 2021

Create View Vw_Producto_mas_vendido_2021
as
select top 1 b.nombreProducto, COUNT(1) as Cantidad
from tb_detalle_pedido a inner join tb_producto b 
on a.idproducto = b.idproducto inner join tb_pedido c
on a.idpedido = c.IdPedido 
where year(c.FechaPedido) = 2021
group by b.nombreProducto
order by Cantidad desc

select * from Vw_Producto_mas_vendido_2021

-- 6.	Se�ale el nombre o nombres de los clientes que no han efectuado 
--		compras en meses de un a�o determinado (2020), cuales son estos meses.

alter view Vw_Clientes_Sin_Pedido_2020
AS

select temp.IdCliente, temp.Nombre_empresa, temp.descripcion AS mes, Cantidad = isnull(t.Cantidad,0)
from	(select a.IdCliente, a.Nombre_empresa, b.idMes, b.descripcion
		from tb_cliente a cross join fn_Meses_2() b) temp Left join (
											select a.IdCliente, b.Nombre_empresa, MONTH(FechaPedido) as Mes, Cantidad = COUNT(1)
											from tb_pedido a inner join tb_cliente b
											on a.IdCliente = b.IdCliente 
											where YEAR(a.FechaPedido) = 2020
											group by a.IdCliente, b.Nombre_empresa, MONTH(FechaPedido) ) t 
on temp.IdCliente = t.IdCliente and temp.idMes = t.Mes
where t.IdCliente is null

--
select * from Vw_Clientes_Sin_Pedido_2020

---Creando la funci�n Tipo Tabla
CREATE FUNCTION fn_Meses_2()
Returns @ctable table (
	idMes int, descripcion varchar(50) 
)
As 
begin

--declare @ctable table ( idMes int, descripcion varchar(50) )

insert into @ctable
(idMes, descripcion)
Values(1, 'enero'), (2,'febrero'), (3,'marzo'), (4,'abril')
, (5,'mayo'), (6,'junio'), (7,'julio'), (8,'agosto')
, (9,'septiembre'), (10,'octubre'), (11,'noviembre'), (12,'diciembre')

return;

end