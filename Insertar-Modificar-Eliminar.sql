USE BD_PEDIDO
GO

/* DML:
	Las consultas SQL de tipo DML (Data Manipulation Language � Lenguaje de manimulaci�n 
	de datos) son para:
	*Insert:	A�adir filas de datos a una tabla.
	*Delete:	Eliminar filas de datos de una tabla.
	*Update:	Modificar los datos de una tabla.
	*Select:	Recuperar datos de una tabla.
	Commit:		Confirmar como permanentes las modificaciones realizadas.
	Rollback:	Deshacer todas las modificaciones realizadas desde la �ltima confirmaci�n.
	
*/


/*USO INSERT*/
-- 1. Insertar una nueva empresa de envio, considerar la empresa DHL. 
declare @idEmpresaEnvio_MAX int
set @idEmpresaEnvio_MAX = (SELECT MAX(idEmpresaEnvio) from tb_empresa_envio tee) + 1

INSERT into tb_empresa_envio
	(idEmpresaEnvio, nombre_empresa, telefono)
values
	(@idEmpresaEnvio_MAX,'DHL','(503) 555-9831')

SELECT * from tb_empresa_envio tee 

-- 2. Insertar graficamente una nueva categoria, considerar la categoria Dulces.
SELECT * from tb_categoria tc	
	
/*USO UPDATE*/
-- 1. Actualizar la empresa de envio "DHL SAC" a "DHL SAC"
select *from tb_empresa_envio tee

update tb_empresa_envio 
	set nombre_empresa = 'DHL SAC' 
		where idEmpresaEnvio = 4 

-- 2. Actualizar graficamente la categoria "Dulces" a "Dulces y Arreglos".
SELECT * from tb_categoria tc
		
/*USO DELETE*/
-- 1. Eliminar la empresa de envio "DHL SAC".
delete from tb_empresa_envio
	where idEmpresaEnvio = 4

select *from tb_empresa_envio tee	
	
-- 2. Eliminar graficamente la categoria "Dulces y Arreglos".
SELECT * from tb_categoria tc

