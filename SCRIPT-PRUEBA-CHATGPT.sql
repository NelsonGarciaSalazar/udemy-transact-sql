/*
CREATE DATABASE PRUEBA_CHATGPT
GO
*/

USE PRUEBA_CHATGPT
GO

-- Verificar y eliminar procedimientos almacenados
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'InsertarCliente', N'P'))
    DROP PROCEDURE IF EXISTS InsertarCliente;
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ObtenerClientePorID', N'P'))
    DROP PROCEDURE IF EXISTS ObtenerClientePorID;
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ObtenerTodosLosClientes', N'P'))
    DROP PROCEDURE IF EXISTS ObtenerTodosLosClientes;
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ActualizarCliente', N'P'))
    DROP PROCEDURE IF EXISTS ActualizarCliente;
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'EliminarCliente', N'P'))
    DROP PROCEDURE IF EXISTS EliminarCliente;
GO

-- Verificar y eliminar funci�n escalar
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ObtenerSiguienteClienteID', N'FN'))
    DROP FUNCTION IF EXISTS ObtenerSiguienteClienteID;
GO

-- Verificar y eliminar secuencia
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ClienteID_Sequence', N'SO'))
    DROP SEQUENCE IF EXISTS ClienteID_Sequence;
GO
-- Verificar y eliminar tablas
DROP TABLE IF EXISTS Referencias;
DROP TABLE IF EXISTS Direcciones;
DROP TABLE IF EXISTS TiposDireccion;
DROP TABLE IF EXISTS Clientes;
GO


-- Creaci�n de la tabla Clientes
CREATE TABLE Clientes (
    ClienteID INT PRIMARY KEY,
    Nombre NVARCHAR(100),
    ApellidoPaterno NVARCHAR(100),
    ApellidoMaterno NVARCHAR(100),
    CorreoElectronico NVARCHAR(150),
    FechaNacimiento DATE
)
GO

-- Creaci�n de la tabla TiposDireccion
CREATE TABLE TiposDireccion (
    TipoDireccionID INT PRIMARY KEY,
    Tipo NVARCHAR(50)
)
GO

-- Creaci�n de la tabla Direcciones
CREATE TABLE Direcciones (
    DireccionID INT PRIMARY KEY,
    ClienteID INT,
    TipoDireccionID INT,
    Direccion NVARCHAR(200),
    Ciudad NVARCHAR(100),
    Estado NVARCHAR(50),
    CodigoPostal NVARCHAR(10),
    Pais NVARCHAR(100),
    FOREIGN KEY (ClienteID) REFERENCES Clientes(ClienteID),
    FOREIGN KEY (TipoDireccionID) REFERENCES TiposDireccion(TipoDireccionID)
)
GO

-- Creaci�n de la tabla Referencias
CREATE TABLE Referencias (
    ReferenciaID INT PRIMARY KEY,
    ClienteReferenteID INT, -- Cliente que hace la referencia
    ClienteReferidoID INT,  -- Cliente que es referido
    FechaReferencia DATE,
    FOREIGN KEY (ClienteReferenteID) REFERENCES Clientes(ClienteID),
    FOREIGN KEY (ClienteReferidoID) REFERENCES Clientes(ClienteID)
)
GO

INSERT INTO Clientes (ClienteID, Nombre, ApellidoPaterno, ApellidoMaterno, CorreoElectronico, FechaNacimiento)
VALUES
    (1, 'Juan', 'Garc�a', 'P�rez', 'juan@example.com', '1990-05-15'),
    (2, 'Mar�a', 'L�pez', 'S�nchez', 'maria@example.com', '1988-10-20'),
    (3, 'Luis', 'Mart�nez', 'G�mez', 'luis@example.com', '1995-03-08'),
    (4, 'Ana', 'Rodr�guez', 'Hern�ndez', 'ana@example.com', '1992-09-12'),
    (5, 'Carlos', 'Fern�ndez', 'Ram�rez', 'carlos@example.com', '1985-07-30'),
    (6, 'Laura', 'D�az', 'Torres', 'laura@example.com', '1998-11-05'),
    (7, 'Jos�', 'P�rez', 'Vargas', 'jose@example.com', '1993-02-18'),
    (8, 'Sof�a', 'G�mez', 'L�pez', 'sofia@example.com', '1987-04-25'),
    (9, 'Daniel', 'Ram�rez', 'Morales', 'daniel@example.com', '1991-06-10'),
    (10, 'Mariana', 'Hern�ndez', 'Sosa', 'mariana@example.com', '1994-12-22'),
    (11, 'Andr�s', 'Torres', 'Fern�ndez', 'andres@example.com', '1989-08-17'),
    (12, 'Valentina', 'S�nchez', 'D�az', 'valentina@example.com', '1997-01-14'),
    (13, 'Javier', 'Vargas', 'Garc�a', 'javier@example.com', '1986-03-04'),
    (14, 'Isabella', 'Morales', 'Mart�nez', 'isabella@example.com', '1999-09-28'),
    (15, 'Emilia', 'L�pez', 'P�rez', 'emilia@example.com', '1996-07-07');
GO

	INSERT INTO TiposDireccion (TipoDireccionID, Tipo)
VALUES
    (1, 'Env�o'),
    (2, 'Facturaci�n'),
    (3, 'Trabajo'),
    (4, 'Vacaciones'),
    (5, 'Otro');
GO

INSERT INTO Direcciones (DireccionID, ClienteID, TipoDireccionID, Direccion, Ciudad, Estado, CodigoPostal, Pais)
VALUES
    (1, 1, 1, 'Calle 123, Colonia A', 'Ciudad A', 'Estado A', '12345', 'Pa�s A'),
    (2, 1, 2, 'Avenida XYZ, Colonia B', 'Ciudad A', 'Estado A', '54321', 'Pa�s A'),
    (3, 2, 1, 'Calle 456, Colonia X', 'Ciudad B', 'Estado B', '67890', 'Pa�s B'),
    (4, 3, 3, 'Oficina 789, Edificio C', 'Ciudad C', 'Estado C', '98765', 'Pa�s C'),
    (5, 4, 1, 'Residencial 456, Urbanizaci�n Y', 'Ciudad D', 'Estado D', '23456', 'Pa�s D'),
    (6, 5, 2, 'Apartado Postal 789, Zona Z', 'Ciudad E', 'Estado E', '87654', 'Pa�s E'),
    (7, 6, 4, 'Casa de Playa 123, Playa ABC', 'Ciudad F', 'Estado F', '45678', 'Pa�s F'),
    (8, 7, 1, 'Av. Principal 789, Conjunto G', 'Ciudad G', 'Estado G', '56789', 'Pa�s G'),
    (9, 8, 2, 'Avenida Central 456, Centro H', 'Ciudad H', 'Estado H', '34567', 'Pa�s H'),
    (10, 9, 3, 'Oficina 101, Edificio I', 'Ciudad I', 'Estado I', '76543', 'Pa�s I'),
    (11, 10, 1, 'Calle 111, Colonia J', 'Ciudad J', 'Estado J', '12345', 'Pa�s J'),
    (12, 11, 2, 'Avenida 222, Barrio K', 'Ciudad K', 'Estado K', '54321', 'Pa�s K'),
    (13, 12, 1, 'Calle 333, Colonia L', 'Ciudad L', 'Estado L', '67890', 'Pa�s L'),
    (14, 13, 2, 'Avenida 444, Zona M', 'Ciudad M', 'Estado M', '98765', 'Pa�s M'),
    (15, 14, 3, 'Oficina 505, Edificio N', 'Ciudad N', 'Estado N', '23456', 'Pa�s N');
GO

INSERT INTO Referencias (ReferenciaID, ClienteReferenteID, ClienteReferidoID, FechaReferencia)
VALUES
    (1, 1, 2, '2023-08-01'),
    (2, 1, 3, '2023-08-02'),
    (3, 2, 5, '2023-08-05'),
    (4, 3, 6, '2023-08-08'),
    (5, 4, 7, '2023-08-10'),
    (6, 4, 8, '2023-08-12'),
    (7, 5, 9, '2023-08-15'),
    (8, 6, 10, '2023-08-18'),
    (9, 7, 12, '2023-08-20'),
    (10, 8, 14, '2023-08-22'),
    (11, 9, 15, '2023-08-25'),
    (12, 10, 1, '2023-08-28'),
    (13, 11, 3, '2023-08-30'),
    (14, 12, 5, '2023-09-02'),
    (15, 13, 7, '2023-09-05');
GO

CREATE FUNCTION ObtenerSiguienteClienteID()
RETURNS INT
AS
BEGIN
    DECLARE @SiguienteID INT;

    SELECT @SiguienteID = ISNULL(MAX(ClienteID), 0) + 1
    FROM Clientes;

    RETURN @SiguienteID;
END;
GO

CREATE PROCEDURE InsertarCliente
    @Nombre NVARCHAR(100),
    @ApellidoPaterno NVARCHAR(100),
    @ApellidoMaterno NVARCHAR(100),
    @CorreoElectronico NVARCHAR(150),
    @FechaNacimiento DATE
AS
BEGIN
    DECLARE @NuevoClienteID INT;
    SET @NuevoClienteID = dbo.ObtenerSiguienteClienteID();

    INSERT INTO Clientes (ClienteID, Nombre, ApellidoPaterno, ApellidoMaterno, CorreoElectronico, FechaNacimiento)
    VALUES (@NuevoClienteID, @Nombre, @ApellidoPaterno, @ApellidoMaterno, @CorreoElectronico, @FechaNacimiento);
END;
GO

CREATE PROCEDURE ObtenerClientePorID
    @ClienteID INT
AS
BEGIN
    SELECT ClienteID, Nombre, ApellidoPaterno, ApellidoMaterno, CorreoElectronico, FechaNacimiento
    FROM Clientes
    WHERE ClienteID = @ClienteID;
END
GO

CREATE PROCEDURE ObtenerTodosLosClientes
AS
BEGIN
    SELECT ClienteID, Nombre, ApellidoPaterno, ApellidoMaterno, CorreoElectronico, FechaNacimiento
    FROM Clientes;
END;
GO

CREATE PROCEDURE ActualizarCliente
    @ClienteID INT,
    @Nombre NVARCHAR(100),
    @ApellidoPaterno NVARCHAR(100),
    @ApellidoMaterno NVARCHAR(100),
    @CorreoElectronico NVARCHAR(150)
AS
BEGIN
    UPDATE Clientes
    SET Nombre = @Nombre, ApellidoPaterno = @ApellidoPaterno, ApellidoMaterno = @ApellidoMaterno, CorreoElectronico = @CorreoElectronico
    WHERE ClienteID = @ClienteID;
END;
GO

CREATE PROCEDURE EliminarCliente
    @ClienteID INT
AS
BEGIN
    DELETE FROM Clientes WHERE ClienteID = @ClienteID;
END;
GO


-- Declarar los par�metros
DECLARE @Nombre NVARCHAR(100) = 'Nuevo';
DECLARE @ApellidoPaterno NVARCHAR(100) = 'Cliente';
DECLARE @ApellidoMaterno NVARCHAR(100) = 'Autogenerado';
DECLARE @CorreoElectronico NVARCHAR(150) = 'nuevo@cliente.com';
DECLARE @FechaNacimiento DATE = GETDATE(); -- Puedes cambiar esto a una fecha espec�fica

-- Ejecutar el procedimiento InsertarCliente
EXEC InsertarCliente
    @Nombre,
    @ApellidoPaterno,
    @ApellidoMaterno,
    @CorreoElectronico,
    @FechaNacimiento;
