use [BD_PEDIDO]
go

/*  TABLAS TEMPORALES
Existen dos tipos de tablas Temporales:

1. #Locales: Las tablas temporales locales tienen una # (Numeral) como primer car�cter en su nombre 
		y s�lo se pueden utilizar en la conexi�n en la que el usuario las crea. Cuando la conexi�n 
		termina la tabla temporal desaparece.

2. ##Globales: Las tablas temporales globales comienzan con ## y son visibles por cualquier usuario 
			   conectado al SQL Server.Sin embargo, esta desaparece cuando el usuario que la creo cierra la conexi�n.

Estas tablas temporales se crean por defecto temporalmente en la base de datos "tempdb".
*/

-- Modo 1: Crear Temporal Local
SELECT *
into #Temporal_01
FROM tb_categoria

select * 
from #Temporal_01

-- Modo 2: Crear temporal Global
SELECT *
into ##Temporal_02
FROM tb_categoria

-- Crear tabla temporal.
Create table #temporal_ejemplo (
id int identity primary key,
nombre varchar(100),
apellido varchar(100)
)

insert into #temporal_ejemplo 
(nombre, apellido)
values( 'roberto','perez')

select * 
from #temporal_ejemplo

--Ejemplos:
select *
into #tb_categoria
from tb_categoria

alter table #tb_categoria
add nombre_limpio varchar(200)

-- Nota: El campo descripci�n tiene como tipo de datos "Text". Por ello, 
		--se debe realizar un Cast( varchar ) para que permita aplicar funciones de Texto.

update #tb_categoria
set nombre_limpio = replace(upper(replace(Cast(descripcion as varchar(max)),',','')),',','-')

select * 
from #tb_categoria