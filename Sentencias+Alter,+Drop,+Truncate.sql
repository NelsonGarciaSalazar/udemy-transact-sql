USE BD_PEDIDO
GO

/* DML: 
	Las consultas SQL de tipo DDL (Data definition Language � Lenguaje de definici�n de datos)
	son las consultas SQL para:
	*Create: Crear un objeto (Tabla, Procedimiento, Funci�n, etc.)
	*Alter: Alterar o Recompilar un objeto (Tabla, Procedimiento, Funci�n, etc.)
	*Truncate: Eliminar toda la informaci�n de una tabla. 
	*Drop: Eliminar un Objeto (BD, Tabla, Procedimiento, Funci�n, etc.)
	Revoke: revocar o quitar los privilegios de un usuario o rol de la base de datos.
	Grant: 	Conceder privilegios, roles, a un usuario o a otro rol.
*/

Create Table TB_EJEMPLO(
ID INT IDENTITY PRIMARY KEY,
DESCRIPCION VARCHAR(100)
);

INSERT INTO TB_EJEMPLO 
(DESCRIPCION) 
VALUES ('EJEMPLO 1'), ('EJEMPLO 2');

SELECT * 
FROM TB_EJEMPLO

/*USO DE ALTER */
ALTER table TB_EJEMPLO
add ESTADO bit

ALTER table TB_EJEMPLO 
alter column ESTADO varchar(2)

/*USO DE TRUNCATE*/
truncate table TB_EJEMPLO

/*USO DE DROP */
drop table if exists tb_ejemplo

drop table TB_EJEMPLO 









