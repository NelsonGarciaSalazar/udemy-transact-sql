/* 
Proceso para quitar datos duplicados usando tablas temporales.
-------------------------------------------------------------
-  Se desea eliminar todos los registros duplicados de los clientes, 
	para quedarse con su �ltima informaci�n estable, para ello, 
	se requiere primero realizar un proceso de Data Quality para identificar estos registros duplicados.
*/
-- Ã‘ --- �
---Ã‘

ALTER TABLE [TABLA_CLIENTE_BASE] 
ADD CORRELATIVO INT IDENTITY PRIMARY KEY

ALTER TABLE [TABLA_CLIENTE_BASE] 
ADD ESTADO VARCHAR(100) 

ALTER TABLE [TABLA_CLIENTE_BASE] 
ADD CLIENTE_NUEVO VARCHAR(100) , DIRECCION_NUEVA VARCHAR(100)

ALTER TABLE [TABLA_CLIENTE_BASE] 
ALTER COLUMN DIRECCION_NUEVA  VARCHAR(500)

UPDATE [TABLA_CLIENTE_BASE] 
SET CLIENTE_NUEVO = CLIENTE,
DIRECCION_NUEVA = DIRECCION_1_C

--- Aplicar limpieza de datos.

Alter FUNCTION fn_limpiarDatos(
@Cadena varchar(500)
) 
returns varchar(500)
as
begin
set @Cadena = LTRIM(RTRIM(@Cadena))
set @Cadena = REPLACE(@Cadena, 'Ã‘','�')
set @Cadena = case when  RIGHT(@Cadena,1) = '.'
						then SUBSTRING(@Cadena,1,len(@Cadena)-1) else @Cadena end
set @Cadena = REPLACE(@Cadena, '#','Nro ')
set @Cadena = REPLACE(@Cadena,'//',' ')

set @Cadena = REPLACE(@Cadena,'  ',' ')

return @Cadena
end

SELECT CLIENTE_NUEVO, dbo.fn_limpiarDatos(CLIENTE_NUEVO)
FROM [dbo].[TABLA_CLIENTE_BASE]
where estado is null 
 
update [TABLA_CLIENTE_BASE]
set estado = 'Vacio' 
where CLIENTE_NUEVO = ''

SELECT DIRECCION_NUEVA, dbo.fn_limpiarDatos(DIRECCION_NUEVA)
FROM [dbo].[TABLA_CLIENTE_BASE]
where estado is null -- AND DIRECCION_NUEVA LIKE '%//%'

update [TABLA_CLIENTE_BASE]
set CLIENTE_NUEVO = dbo.fn_limpiarDatos(CLIENTE_NUEVO),
DIRECCION_NUEVA= dbo.fn_limpiarDatos(DIRECCION_NUEVA)
where estado is null 

------ AGRUPAR: Buscar Duplicidad
drop table if exists #AGRUPADO_1;
SELECT max(CORRELATIVO) as correlativo, CLIENTE_NUEVO, DIRECCION_NUEVA, COUNT(1) CANTIDAD -- 135 
INTO #AGRUPADO_1
FROM [TABLA_CLIENTE_BASE]
GROUP BY CLIENTE_NUEVO, DIRECCION_NUEVA

--marcar unicos
UPDATE [TABLA_CLIENTE_BASE]
SET ESTADO = 'Unico'
where estado is null 
and CORRELATIVO in ( select CORRELATIVO  from #AGRUPADO_1 Where CANTIDAD = 1 )

delete from #AGRUPADO_1
where CANTIDAD = 1

--select * from #AGRUPADO_1 
-- select * from [TABLA_CLIENTE_BASE] 
--- marcar duplicados
UPDATE [TABLA_CLIENTE_BASE]
SET ESTADO = 'Duplicado'
where estado is null 
and not CORRELATIVO in ( select CORRELATIVO  from #AGRUPADO_1 )

-- marcar unicos
UPDATE [TABLA_CLIENTE_BASE]
SET ESTADO = 'Unico'
where estado is null 
and CORRELATIVO in ( select CORRELATIVO  from #AGRUPADO_1 )
-----------------------------------------------
--- volver a empezar: 
/*
update [TABLA_CLIENTE_BASE]
set ESTADO = null
*/
------------------------------------------------
SELECT CLIENTE_NUEVO,  COUNT(1) CANTIDAD -- 131 
FROM [TABLA_CLIENTE_BASE]
GROUP BY CLIENTE_NUEVO

select estado, COUNT(1) cantidad
from [TABLA_CLIENTE_BASE]
group by estado


select CLIENTE_NUEVO, COUNT(1) as cantidad 
into #revisar
from [TABLA_CLIENTE_BASE] 
where ESTADO = 'unico'
group by CLIENTE_NUEVO 
having COUNT(1) > 1

drop table if exists #depurar;
select CORRELATIVO, CLIENTE_NUEVO, DIRECCION_NUEVA, LEN(DIRECCION_NUEVA) as longitud
into #depurar
from [TABLA_CLIENTE_BASE] 
where ESTADO = 'unico' and
CLIENTE_NUEVO in ( select CLIENTE_NUEVO from #revisar )

drop table if exists #longitudMaxima;
select CLIENTE_NUEVO, min(longitud) as longitud
into #longitudMaxima
from ( 
	select CLIENTE_NUEVO, DIRECCION_NUEVA, LEN(DIRECCION_NUEVA) as longitud
	from [TABLA_CLIENTE_BASE] 
	where ESTADO = 'unico' and
	CLIENTE_NUEVO in ( select CLIENTE_NUEVO from #revisar )
) as t 
group by CLIENTE_NUEVO
 

UPDATE [TABLA_CLIENTE_BASE]
SET ESTADO = 'Duplicado'
where CORRELATIVO in ( 
select CORRELATIVO
from #depurar a inner join #longitudMaxima b
on a.CLIENTE_NUEVO = b.CLIENTE_NUEVO and a.longitud = b.longitud ) 
go


SELECT ESTADO, COUNT(1) AS CANTIDAD
FROM [TABLA_CLIENTE_BASE] 
GROUP BY ESTADO WITH ROLLUP



