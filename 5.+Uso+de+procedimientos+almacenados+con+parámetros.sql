use BD_PEDIDO
go

/*
5. Uso de procedimientos almacenados c-s par�metros
-----------------------------------------------------
Los Procedimientos almacenados son muy  usados en el dise�o de la base de datos, 
pues estos permiten agilizar los procesos de consultas de datos, aumentar la seguridad, 
reutilizar c�digo y permiten desarrollo de software m�s �gil.
*/

-- PROCEDIMIENTOS ALMACENADOS CON PARAMETROS

--1. Listar Cliente enviar como parametro su ID

Create Proc Usp_Listar_cliente_x_ID
@IdCliente int
as
SELECT c.IdCliente, c.Nombre_empresa, c.Direccion
FROM tb_cliente c 
where c.IdCliente = @IdCliente
go

exec Usp_Listar_cliente_x_ID @IdCliente = 11

--2. Listar todos los pedidos enviando como parametro el a�o y el mes del pedido.

Create proc Usp_pedido_x_anio_mes
@Anio int,
@Mes int
As
select *
from tb_pedido 
where year(FechaPedido) = @Anio and MONTH(FechaPedido) = @Mes
go

exec Usp_pedido_x_anio_mes 2020, 8
exec Usp_pedido_x_anio_mes @Anio=2020, @Mes=8

--3. Listar todos los clientes que realizaron pedidos entre un rango de fecha enviado como parametro.

Alter proc Usp_ListaPedidosCliente_RangoFechas
@cFechainicio Varchar(10),
@cFechaFin Varchar(10)
as 

set dateformat dmy

SELECT b.IdCliente, b.Nombre_empresa, a.FechaPedido
FROM tb_pedido a inner join tb_cliente b 
on a.IdCliente = b.IdCliente
where a.FechaPedido between @cFechainicio and @cFechaFin 
order by a.FechaPedido

--exec Usp_ListaPedidosCliente_RangoFechas '2019-07-01', '2019-12-31'

exec Usp_ListaPedidosCliente_RangoFechas '01-07-2019', '15-08-2019'


--4. Listar el empleado que vendio m�s, enviando como parametro el a�o, mes.

set language spanish

Alter proc Usp_EmpleadoDelMes 
@Anio int,
@Mes int 
AS
/*
select top 1 a.IdEmpleado,a.Nombre, a.Apellidos, YEAR(b.FechaPedido) as [A�o], Mes = MONTH(b.FechaPedido) ,COUNT(b.IdPedido) as Cantidad
from tb_empleado a inner join tb_pedido b 
on a.IdEmpleado = b.IdEmpleado 
Where YEAR(b.FechaPedido) = @Anio and MONTH(b.FechaPedido) = @Mes
group by  a.IdEmpleado, a.Nombre, a.Apellidos, YEAR(b.FechaPedido), MONTH(b.FechaPedido)
order by Cantidad Desc 
*/
select top 1 a.IdEmpleado,a.Nombre, a.Apellidos, @Anio as [A�o], Mes = @Mes ,COUNT(b.IdPedido) as Cantidad
from tb_empleado a inner join tb_pedido b 
on a.IdEmpleado = b.IdEmpleado 
Where YEAR(b.FechaPedido) = @Anio and MONTH(b.FechaPedido) = @Mes
group by  a.IdEmpleado, a.Nombre, a.Apellidos
order by Cantidad Desc 

exec Usp_EmpleadoDelMes 2021, 4

--5. Colocar un estado a todos los pedidos que superaron los d�as de entrega ingresado como parametro. (15)

CREATE PROC USP_ESTADO_ENTREGA_PEDIDO
@cCantidadDias int
as
select  T.IdPedido, T.FechaEnvio, FechaEntrega, 
Estado = Case when ISNULL(Diferencia_dias,0) > @cCantidadDias  then 'SUPERA' ELSE 'CUMPLE' END
from (
			select IdPedido, FechaEnvio, FechaEntrega, DATEDIFF(day, FechaEnvio, FechaEntrega) Diferencia_dias
			from tb_pedido ) as T 
go


exec USP_ESTADO_ENTREGA_PEDIDO 15
 



--6. Crear un procedimiento que realice un insert, update o delete de una categoria enviando como parametro 
--   la acci�n a realizar.

ALTER PROC USP_MANTENIMIENTO_ACCION 
@cAccion CHAR(3) ,
@cidcategoria int, 
@cnombrecategoria varchar (100),
@cdescripcion text
As

IF @cAccion = 'INS' -- INSERT
BEGIN

DECLARE @cNuevoidcategoria int 
Set @cNuevoidcategoria = isnull(( Select max(idcategoria) from tb_categoria),0) + 1

	INSERT INTO tb_categoria 
	(idcategoria, nombrecategoria, descripcion) 
	VALUES (@cNuevoidcategoria, @cnombrecategoria, @cdescripcion)

	PRINT 'Insertar'
END 
ELSE IF @cAccion = 'UPD' -- UPDATE
BEGIN
	UPDATE tb_categoria
	SET nombrecategoria = @cnombrecategoria,
	descripcion = @cdescripcion
	WHERE idcategoria = @cidcategoria
	PRINT 'Actualizar'
END
ELSE IF @cAccion = 'DEL' -- delete
BEGIN 
	delete tb_categoria
	WHERE idcategoria = @cidcategoria
	PRINT 'Eliminar'
END
GO

exec USP_MANTENIMIENTO_ACCION 'INS',69999,'NEW CATEGORY','NUEVA CATEGORIA DE PRUEBA'

exec USP_MANTENIMIENTO_ACCION 'UPD',10,'Categoria de Helados','Helados de todos los sabores'

exec USP_MANTENIMIENTO_ACCION 'DEL',10,'',''

select * from tb_categoria


--7. Crear un procedimiento que evalue si existe una categoria y que te devuelva un mensaje "Si existe" o
--   "No Existe" enviando como parametro la descripci�n de la categoria.


ALTER PROC USP_VALIDAR_EXISTENCIA
@cnombrecategoria varchar(100)
as

declare @cResultado varchar(25)
set @cResultado = 'No Existe'

IF exists ( select 1 from tb_categoria where nombrecategoria = @cnombrecategoria )
begin
	set @cResultado = 'Existe'
end

SELECT @cResultado AS RESULTADO
go


exec USP_VALIDAR_EXISTENCIA 'Carnes'


