use [BD_PEDIDO]
go

/*Consultas Usando Left Join y Right Join*/

-- LEFT JOIN:
-- 1. Mostrar empleados que no hayan realizado ning�n pedido. 

select a.IdEmpleado, a.Apellidos, b.IdEmpleado, b.IdPedido
	from tb_empleado a 
		left join tb_pedido b
					on a.IdEmpleado = b.IdEmpleado 
		where b.IdEmpleado is null
		

Insert into tb_empleado
	([IdEmpleado], [Apellidos], [Nombre]) 
values
	(10, 'Perez Bazan', 'Roberto')

select MAX([IdEmpleado]) from tb_empleado

-- 2. Mostrar clientes que no han realizado pedidos en el 2021

SELECT a.IdCliente, a.Nombre_empresa, b.FechaPedido, ' Resultado: ' + 
		case 
				when b.FechaPedido is null then 'No se encontro Fecha Pedido' 
			else Cast(YEAR(b.FechaPedido) as varchar(4)) 
		end
	FROM tb_cliente a 
		left join tb_pedido b
					on a.IdCliente = b.IdCliente 
					and YEAR(b.FechaPedido) = 2021
		where b.IdCliente is null

-- RIGHT JOIN 
-- 1. Mostrar categorias que no contengan productos.

select a.nombreProducto, b.nombrecategoria
from tb_producto a right join tb_categoria b
on a.idCategoria = b.idcategoria
where a.idCategoria is null 

insert into tb_categoria 
(idcategoria, nombrecategoria, descripcion) 
values (9, 'Nueva Categoria', 'Ejemplo')
select MAX(idcategoria) from tb_categoria


-- 2. Mostrar todos los proveedores que contengan igual o mayor de 4 productos.
 select  
 Case when isnull(a.cantidad,0) >= 4 then 'Cumple' Else 'No Cumple' End As Estado ,
b.nombre_empresa, isnull(a.cantidad,0) As Cantidad_Productos
 from (
		 select idProveedor, COUNT(1) as cantidad
		 from tb_producto 
		 group by idProveedor ) a Right Join tb_proveedor b
		 on a.idProveedor = b.idProveedor 


		 INSERT INTO  tb_proveedor
		 ([idProveedor], [nombre_empresa],[cargocontacto]) 
		 VALUES(30, 'netflix', 'juan perez')

		 SELECT MAX([idProveedor]) FROM tb_proveedor