use BD_PEDIDO
go

/*
5. Uso de procedimientos almacenados c-s par�metros
-----------------------------------------------------
Los Procedimientos almacenados son muy  usados en el dise�o de la base de datos, 
pues estos permiten agilizar los procesos de consultas de datos, aumentar la seguridad, 
reutilizar c�digo y permiten desarrollo de software m�s �gil.
*/

-- PROCEDIMIENTOS ALMACENADOS SIN PARAMETROS

--1. Listar todas las categor�as ordenados por descripci�n.

CREATE PROC USP_Listar_Categoria
as
SELECT c.idcategoria, c.descripcion
FROM tb_categoria c

EXEC USP_Listar_Categoria 

--2. Listar todos los clientes ordenados por nombre

DROP PROC IF EXISTS USP_Listar_Clientes
GO

CREATE PROCEDURE USP_Listar_Clientes
as
SELECT c.IdCliente, c.Nombre_empresa, c.Ciudad, c.Direccion
FROM tb_cliente c
GO

EXEC USP_Listar_Clientes
GO

--3. Listar todos los proveedores con su cantidad de productos.

CREATE PROC USP_CantidadProducto_Proveedor
as
SELECT b.idProveedor, b.nombre_empresa, COUNT(1) as Cantidad
FROM tb_producto a inner join tb_proveedor b 
on a.idProveedor = b.idProveedor 
Group by b.idProveedor, b.nombre_empresa
order by Cantidad desc
go

exec USP_CantidadProducto_Proveedor

--4. Listar todos los productos con su categoria.

Create proc USP_Productos_Categoria
As
select a.idproducto, a.nombreProducto, b.nombrecategoria
from tb_producto a inner join tb_categoria b 
on a.idCategoria = b.idcategoria

exec USP_Productos_Categoria

