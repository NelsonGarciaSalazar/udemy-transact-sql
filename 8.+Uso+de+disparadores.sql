/*
	8. Uso de disparadores(Triggers)

�Que es un disparador ?
Un disparador o trigger son procedimientos almacenados especiales, los cuales se ejecuta autom�ticamente 
cuando se realiza una determina acci�n en la base de datos o en el servidor de base de datos:

Existen tres tipos de disparadores:
1. Disparadores DML(Lenguaje de manipulaci�n de datos): 
	- Cuando se ejecuta una sentencia INSERT, UPDATE o DELETE.  (*)

2. Disparadores DDL(Lenguaje de definici�n de datos): 
	- Cuando se ejecuta una sentencia CREATE, ALTER, DROP, GRANT, DENY, etc.
	- Info: https://docs.microsoft.com/en-us/sql/relational-databases/triggers/ddl-triggers?view=sql-server-ver15
3. Disparadores Logon (Iniciar sesi�n): 
	- Cuando existe un evento de inicio de sesi�n.
	- Info: https://docs.microsoft.com/en-us/sql/relational-databases/triggers/logon-triggers?view=sql-server-ver15
	
------------------------------------------------------
Disparadores DML 
------------------------------------------------------
Existen dos tipos de Disparadores: 

1. After = Se dispara "despu�s" de una determinada acci�n de una tabla (Insert | Update | Delete)
	- Puede ser definido tambi�n como "For".

2. Instead of = Se dispara "antes" de una determinada acci�n de una tabla o vista (Insert | Update | Delete)
*/

/*  IMPORTANTE: 
	Internamente SQLServer genera  2 tablas: "Inserted", "Deleted" cuando se realiza una determinada acci�n 
	de Insert, Update o Delete.
*/

-- Eliminar trigger
DROP TRIGGER IF EXISTS TR_Muestra
GO

--crear un triggers
CREATE TRIGGER TR_Muestra ON [tb_empresa_envio]
AFTER INSERT, UPDATE, DELETE
AS
 SELECT * , 'Es Deleted' As TablaVirtual FROM deleted; 

 SELECT *, 'Es Inserted' As TablaVirtual FROM inserted;
GO

-- Cuando se realiza un Insert 
INSERT INTO [tb_empresa_envio] 
(idEmpresaEnvio, nombre_empresa, telefono)
VALUES 
(99,'Empresa de Microsoft', '987456321')

-- Cuando se realiza un Update 
UPDATE [tb_empresa_envio]
set nombre_empresa = 'Microsoft', 
telefono = '(503) 553-7549' 
where idEmpresaEnvio = 99

-- Cuando se realiza un Delete
delete from [tb_empresa_envio]
where idEmpresaEnvio = 99


-- 1. Disparador de Tipo After o For
--1.1 Crear un Trigger que actualice el stock del producto vendido. 

DROP TRIGGER IF EXISTS TR_ACTUALIZAR_STOCK_INS
GO

CREATE TRIGGER TR_ACTUALIZAR_STOCK_INS ON [dbo].[tb_detalle_pedido]
FOR INSERT
AS
UPDATE A
SET unidadesEnExistencia = (A.unidadesEnExistencia - B.cantidad)
FROM tb_producto A INNER JOIN inserted B 
ON A.idproducto = b.idproducto
GO
/* -- EJECUCI�N PRUEBA INS
SELECT * FROM tb_producto WHERE idproducto = 2

INSERT INTO tb_detalle_pedido 
(idpedido, idproducto, preciounidad, cantidad, descuento)
VALUES(10252, 2, 19.00, 2, 0)

SELECT * FROM tb_producto WHERE idproducto = 2

 */
 DROP TRIGGER IF EXISTS TR_ACTUALIZAR_STOCK_UDP
GO

CREATE TRIGGER TR_ACTUALIZAR_STOCK_UDP ON [dbo].[tb_detalle_pedido]
FOR UPDATE
AS

UPDATE A
SET unidadesEnExistencia = (A.unidadesEnExistencia + B.cantidad)
FROM tb_producto A INNER JOIN deleted B 
ON A.idproducto = b.idproducto

UPDATE A
SET unidadesEnExistencia = (A.unidadesEnExistencia - B.cantidad)
FROM tb_producto A INNER JOIN inserted B 
ON A.idproducto = b.idproducto
GO
 /* -- EJECUCI�N PRUEBA UPD
 SELECT * FROM tb_producto WHERE idproducto = 2

UPDATE [tb_detalle_pedido]
SET cantidad = 3
WHERE idproducto = 2 AND IdPedido = 10252

SELECT * FROM tb_producto WHERE idproducto = 2


*/
 DROP TRIGGER IF EXISTS TR_ACTUALIZAR_STOCK_DEL
GO

CREATE TRIGGER TR_ACTUALIZAR_STOCK_DEL ON [dbo].[tb_detalle_pedido]
FOR DELETE
AS
UPDATE A
SET unidadesEnExistencia = (A.unidadesEnExistencia + B.cantidad)
FROM tb_producto A INNER JOIN deleted B 
ON A.idproducto = b.idproducto
GO

/* -- EJECUCI�N PRUEBA DEL
SELECT * FROM tb_producto WHERE idproducto = 2

DELETE FROM [tb_detalle_pedido]
WHERE idproducto = 2 AND IdPedido = 10252

SELECT * FROM tb_producto WHERE idproducto = 2
*/

/*
INSERT INTO tb_detalle_pedido 
(idpedido, idproducto, preciounidad, cantidad, descuento)
VALUES(10252, 2, 19.00, 3, 0)

SELECT * FROM tb_pedido WHERE IdPedido = 10252
SELECT * FROM tb_detalle_pedido WHERE IdPedido = 10252
SELECT * FROM tb_producto WHERE idproducto = 2
*/


--1.2 Crear un Trigger que registre todos los cambios que se realicen en la tabla productos.

CREATE TABLE [dbo].[tb_producto_Auditoria](
	idAuditoria int identity primary key,
	[idproducto] [int] NOT NULL,
	[nombreProducto_old] [varchar](40) NULL,
	[precioUnidad_old] [decimal](18, 2) NULL,
	[unidadesEnExistencia_old] [smallint] NULL,
	[nombreProducto_New] [varchar](40) NULL,
	[precioUnidad_New] [decimal](18, 2) NULL,
	[unidadesEnExistencia_New] [smallint] NULL,
)

ALTER TABLE [tb_producto_Auditoria] 
ADD ACCION VARCHAR(35)


ALTER TRIGGER TR_PRODUCTO_AUDITORIA ON [dbo].[tb_producto]
AFTER INSERT, UPDATE, DELETE 
AS

IF (( SELECT COUNT(1) FROM inserted) >= 1 AND ( SELECT COUNT(1) FROM deleted ) >= 1 ) -- UPDATE
BEGIN
	
	INSERT INTO [tb_producto_Auditoria]
	(idproducto, nombreProducto_old, precioUnidad_old, unidadesEnExistencia_old, 
	nombreProducto_New, precioUnidad_New, unidadesEnExistencia_New, ACCION) 
	SELECT a.idproducto, a.nombreProducto, a.precioUnidad , a.unidadesEnExistencia,
		     CASE WHEN b.nombreProducto <> a.nombreProducto THEN b.nombreProducto ELSE NULL END, 
			  CASE WHEN b.precioUnidad <> a.precioUnidad THEN b.precioUnidad ELSE NULL END, 
			 CASE WHEN b.unidadesEnExistencia <> a.unidadesEnExistencia THEN b.unidadesEnExistencia ELSE NULL END, 
			 'UPDATE'
	FROM  deleted a inner join inserted b 
	on a.idproducto = b.idproducto

END 
ELSE IF (( SELECT COUNT(1) FROM inserted) >= 1 AND ( SELECT COUNT(1) FROM deleted ) = 0 ) -- INSERT
BEGIN

	INSERT INTO [tb_producto_Auditoria]
	( idproducto, nombreProducto_New, precioUnidad_New, unidadesEnExistencia_New, ACCION) 
	SELECT b.idproducto,  b.nombreProducto, b.precioUnidad,b.unidadesEnExistencia, 'INSERT'
	FROM  inserted b 

END 
ELSE IF (( SELECT COUNT(1) FROM inserted) = 0 AND ( SELECT COUNT(1) FROM deleted ) >= 1 ) -- DELETE
BEGIN

	INSERT INTO [tb_producto_Auditoria]
	( idproducto, nombreProducto_old, precioUnidad_old, unidadesEnExistencia_old, ACCION) 
	SELECT b.idproducto, b.nombreProducto , b.precioUnidad, b.unidadesEnExistencia, 'DELETE'
	FROM  deleted b 
END 
GO

/*Ejemplo al Actualizar*/
UPDATE tb_producto
SET nombreProducto = 'Aniz' 
WHERE idproducto = 1 

--select MAX(idproducto) from tb_producto
/*Ejemplo al Insertar*/
insert into tb_producto 
(idproducto, nombreProducto, idProveedor, idCategoria, cantidadPorUnidad, precioUnidad, 
unidadesEnExistencia, unidadesEnPedido, nivelNuevoPedido, suspendido, categoriaProducto) 
values (78, 'Nuevo Producto prueba', 1,1,1 , 20, 100, 10, 1,0, '')

/*Ejemplo al Eliminar*/
delete from  tb_producto where idproducto = 78

SELECT * FROM [tb_producto_Auditoria]
