USE BD_PEDIDO
GO
 
/* Funciones de Texto  ---
LEFT: Extrae caracteres desde la izquierda.
RIGHT: Extrae caracteres desde la derecha. 
LEN: Contabiliza los caracteres.
LOWER: Muestra en min�scula los textos.
UPPER: Muestra en may�scula los textos. 
REPLACE: Reemplaza valores de Texto.
LTRIM: Quita Espacios vacios de la izquierda de un texto.
RTRIM: Quita Espacios vacios de la derecha de un texto. 
CONCAT: Cancatena cadenas de texto.
SUBSTRING: Extrae caracteres indicandole el inicio y longitud.
SPACE: A�ade espacios a un texto.
TRIM: Elimina espacios de adelante y atr�s del texto.
(Disponible en versi�n SQL Server 2017 (14.x) en adelante)
*/
 
DECLARE @NOMBRE VARCHAR(100)
SET @NOMBRE = 'JUAN PEREZ'

--SPACE: A�ade espacios a un texto.
--SELECT @NOMBRE + SPACE(1) + 'MORALES'

--SUBSTRING: Extrae caracteres indicandole el inicio y longitud.
SELECT SUBSTRING(@NOMBRE, 3, 5)

--CONCAT: Cancatena cadenas de texto.
SELECT CONCAT(@NOMBRE, ' MORALES', ' - JPM')

--RTRIM: Quita Espacios vacios de la derecha de un texto. 
SELECT RTRIM(@NOMBRE)

--LTRIM: Quita Espacios vacios de la izquierda de un texto.
SELECT LTRIM(@NOMBRE)

---REPLACE: Reemplaza valores de Texto.
SELECT REPLACE(REPLACE(@NOMBRE, ' ','-'),'p','P')

--UPPER: Muestra en may�scula los textos. 
SELECT UPPER(@NOMBRE)

--LOWER: Muestra en min�scula los textos.
SELECT LOWER(@NOMBRE)

--LEN: Contabiliza los caracteres.
SELECT LEN(@NOMBRE)

--RIGHT: Extrae caracteres desde la derecha. 
SELECT RIGHT(@NOMBRE,5)

--LEFT: Extrae caracteres desde la izquierda.
SELECT LEFT(@NOMBRE,5)

/*
Otras funciones:
https://docs.microsoft.com/en-us/sql/t-sql/functions/string-functions-transact-sql?view=sql-server-ver15
*/


/*Funciones de Fecha*/ 
/*
GETDATE():	Devuelve la fecha y la hora actual
DATEPART():	Devuelve una sola parte de una fecha / hora
DATENAME(): Devuelve el nombre de una parte de la fecha
DATEADD():	Suma o resta un intervalo de tiempo especificado desde una fecha
DATEDIFF():	Devuelve el tiempo entre dos fechas
CONVERT():	Muestra datos de fecha / hora en formatos diferentes
*/
 
select GETDATE() ;

SELECT	 DATEPART(year, GETDATE())   as Anio
		,DATEPART(month, GETDATE())   as mes
		,DATEPART(day, GETDATE())  as day
		,DATEPART(dayofyear, GETDATE())   as diadelanio
		,DATEPART(weekday, GETDATE())   as NumeroDiadeSemana


set language english --spanish
select DATENAME(MONTH,getdate()) as NombreMes,
 DATENAME(WEEKDAY,getdate()) as Nombredia


	SELECT GETDATE() FECHA_ACTUAL,
	DATEADD(MM,-4,GETDATE())	As FechaHace4Meses,	---MONTH
	 DATEADD(Q,-2,GETDATE())	 AS Hace2Trimestres,	---QUARTER
	 DATEADD(YYYY,-5,GETDATE()) as Hace5Anos	---YEAR


	DECLARE @start_dt DATETIME,  @end_dt DATETIME
	SET @start_dt = '2020-03-10 00:43:57.370'
	SET @end_dt = GETDATE() 

SELECT 
    DATEDIFF(year, @start_dt, @end_dt) diff_in_year, 
    DATEDIFF(quarter, @start_dt, @end_dt) diff_in_quarter, 
    DATEDIFF(month, @start_dt, @end_dt) diff_in_month, 
    DATEDIFF(dayofyear, @start_dt, @end_dt) diff_in_dayofyear, 
    DATEDIFF(day, @start_dt, @end_dt) diff_in_day, 
    DATEDIFF(week, @start_dt, @end_dt) diff_in_week, 
    DATEDIFF(hour, @start_dt, @end_dt) diff_in_hour, 
    DATEDIFF(minute, @start_dt, @end_dt) diff_in_minute, 
    DATEDIFF(second, @start_dt, @end_dt) diff_in_second;
 

--The number indicate the format to show the date
SELECT CONVERT(varchar, GETDATE(), 13) --28 Mar 2024 13:57:13:467


DECLARE @Contador INT = 0
DECLARE @Fecha DATETIME = getdate()

drop table if exists #ListaFormat
CREATE TABLE #ListaFormat (OpcionFormato int, FechaResult nvarchar(40))

WHILE (@Contador <= 150 )
	BEGIN
	   BEGIN TRY
	      INSERT INTO #ListaFormat
		  		(OpcionFormato, FechaResult)
	      SELECT CONVERT(Nvarchar, @Contador), CONVERT(Nvarchar,@Fecha, @Contador) 
	      SET @Contador = @Contador + 1
	   END TRY	   
	   BEGIN CATCH
	   PRINT '...'
	      SET @Contador = @Contador + 1
	      IF @Contador >= 150
	      BEGIN
	         BREAK
	      END
	   END CATCH
	END

SELECT * FROM #ListaFormat


 